<?php


namespace Alura\Cursos\Controller;


interface InterfaceDeControladorRequisicao
{
    public function processaRequisicao(): void;
}